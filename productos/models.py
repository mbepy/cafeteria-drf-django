# from django.db import models as modelsdj
from djongo import models


# Create your models here.

class Producto(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField(default=0)

    objects = models.DjongoManager()

    class Meta:
        db_table = 'producto'

    def __str__(self):
        return self.nombre
