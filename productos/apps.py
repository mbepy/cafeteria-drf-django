import djongo.models
from django.apps import AppConfig


class ProductosConfig(AppConfig):
    # default_auto_field = 'django.db.models.BigAutoField'
    default_auto_field = 'djongo.models.BigAutoField'
    # default_auto_field = 'djongo.models.ObjectId'
    name = 'productos'
