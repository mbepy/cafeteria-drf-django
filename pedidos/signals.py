from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import Pedido


@receiver(pre_save, sender=Pedido)
def calcular_total(sender, instance, **kwargs):
    total = sum(p.producto.precio * p.cantidad for p in instance.productos)
    instance.total = total