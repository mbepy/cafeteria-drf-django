from django.apps import AppConfig


class PedidosConfig(AppConfig):
    # default_auto_field = 'django.db.models.BigAutoField'
    default_auto_field = 'djongo.models.BigAutoField'
    name = 'pedidos'
