import json
from collections import OrderedDict

from rest_framework import serializers

from .models import Pedido, PedidoProducto
from productos.serializers import ProductoSerializer
from productos.models import Producto


class PedidoProductoSerializer(serializers.Serializer):
    # id = serializers.IntegerField()
    producto = ProductoSerializer()
    cantidad = serializers.IntegerField(default=1)

    class Meta:
        # model = PedidoProducto
        fields = '__all__'


class PedidoSerializer(serializers.ModelSerializer):
    # productos = PedidoProductoSerializer(many=True)

    class Meta:
        model = Pedido
        fields = ('id', 'fecha', 'usuario', 'productos', 'total')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['productos'] = instance.productos
        return data

    # def validate_cantidad(self, cantidad):
    #     if cantidad <= 0:
    #         raise serializers.ValidationError("La cantidad debe ser mayor a cero")
    #     return cantidad

    def validate_productos(self, productos):
        for prod in productos:
            p = prod['producto']
            elements = p.items()
            if not ('id' in p and 'nombre' in p and 'precio' in p and len(elements) == 3):
                raise serializers.ValidationError("insuficientes argumentos")
            ID = p['id']
            if Producto.objects.filter(pk=ID):
                pr = Producto.objects.get(pk=ID)
                if not (pr.nombre == p['nombre'] and pr.precio == p['precio']):
                    raise serializers.ValidationError("Los datos de los campos no coinciden con el producto")
            else:
                raise serializers.ValidationError("id del producto inexistente")
            if not 'cantidad' in prod or prod['cantidad'] <= 0:
                raise serializers.ValidationError("La cantidad debe ser mayor a cero")
        return productos



    # def validate(self, attrs):
    #     # self.validate_productos(attrs.get('productos'))
    #     return super().validate(attrs)

