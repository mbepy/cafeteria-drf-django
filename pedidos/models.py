# from django.db import models
import json

import djongo.models
from django.views.decorators.csrf import csrf_exempt
from djongo import models
from rest_framework.exceptions import ValidationError

# Create your models here.
from productos.models import Producto
from usuarios.models import Usuario
from rest_framework import serializers

class PedidoProducto(models.Model):
    id = models.IntegerField(primary_key=True, default=1)
    producto = models.EmbeddedField(model_container=Producto)
    cantidad = models.IntegerField(default=1)

    class Meta:
        abstract = True
        db_table = 'pedidoproducto'


def format_list(l):
    return [{"value": item} for item in l]


class Pedido(models.Model):
    id = models.IntegerField(primary_key=True)
    fecha = models.DateField(auto_now_add=True)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    # productos = models.ArrayField(
    #     model_container=PedidoProducto,
    #     null=True,
    # )
    productos = models.JSONField(default=list)
    total = models.IntegerField(default=0)

    objects = models.DjongoManager()

    def save(self, *args, **kwargs):
        total = 0
        productos = getattr(self, 'productos')
        for element in productos:
            total += element['producto']['precio']*element['cantidad']
        setattr(self, 'total', total)
        super().save(*args, **kwargs)

    class Meta:
        db_table = 'pedido'

    def __str__(self):
        return str(self.id)
