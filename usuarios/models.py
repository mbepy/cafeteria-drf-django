# from django.db import models
from djongo import models
from django.contrib.auth.models import AbstractUser, Group
from django.utils.translation import gettext_lazy as _
# Create your models here.


class Usuario(AbstractUser):
    USERNAME_FIELD = 'email'
    email = models.EmailField(_("email address"), blank=True)
    # se eliminan los campos username
    username = None
    # related_name="usuarios"
    groups = models.ManyToManyField(
        'auth.Group',
        related_name='usuarios',
        blank=True,
        verbose_name=_('mis grupos')
    )

    user_permissions = models.ManyToManyField(
        'auth.Permission',
        related_name='usuarios',
        blank=True,
        verbose_name=_('mis permisos')
    )

    def __str__(self):
        return self.email

    class Meta:
        abstract = False
        db_table = "usuario_auth"
