import djongo.models
from django.apps import AppConfig


class UsuariosConfig(AppConfig):
    default_auto_field = 'djongo.models.BigAutoField'
    # default_auto_field = 'django.db.models.BigAutoField'
    name = 'usuarios'
