from rest_framework import viewsets

from .models import Usuario
from .serializers import UsuarioSerializer


# Create your views here.
class UsuariosViewSet(viewsets.ModelViewSet):
    serializer_class = UsuarioSerializer
    queryset = Usuario.objects.all()


